﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[ExecuteInEditMode]
public class RandomFoliage : MonoBehaviour
{
	public GameObject[] Prefabs;
	public int HowMany = 20;
	public bool Execute;
	public bool ClearAll;

	void Update()
	{
		if(Execute)
		{
			Execute = false;
			for(int i = 0; i < HowMany; i++)
			{
				GameObject child = PrefabUtility.InstantiatePrefab(Prefabs[Random.Range(0, Prefabs.Length - 1)]) as GameObject;
				child.transform.parent = transform;
				child.transform.position = transform.position + new Vector3(Random.Range(-transform.localScale.x, transform.localScale.x), Random.Range(-transform.localScale.y, transform.localScale.y), 0f);
			}
		}
		if(ClearAll)
		{
			ClearAll = false;
			foreach(Transform ToDes in transform)
			{
				DestroyImmediate(ToDes.gameObject);
			}
		}
	}
	void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawCube(transform.position, new Vector3(transform.localScale.x * 2f, transform.localScale.y * 2f, 1f));
	}
}