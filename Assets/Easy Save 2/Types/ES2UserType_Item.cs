using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ES2UserType_Item : ES2Type
{
	public override void Write(object obj, ES2Writer writer)
	{
		Item data = (Item)obj;
		// Add your writer.Write calls here.
		writer.Write(data.Amount);

	}
	
	public override object Read(ES2Reader reader)
	{
		Item data = new Item();
		Read(reader, data);
		return data;
	}

	public override void Read(ES2Reader reader, object c)
	{
		Item data = (Item)c;
		// Add your reader.Read calls here to read the data into the object.
		data.Amount = reader.Read<System.Int32>();

	}
	
	/* ! Don't modify anything below this line ! */
	public ES2UserType_Item():base(typeof(Item)){}
}