using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ES2UserType_GunSave : ES2Type
{
	public override void Write(object obj, ES2Writer writer)
	{
		GunSave data = (GunSave)obj;
		// Add your writer.Write calls here.
		writer.Write(data.InMag);
		writer.Write(data.BulletDelayRem);
		writer.Write(data.ReloadRem);

	}
	
	public override object Read(ES2Reader reader)
	{
		GunSave data = new GunSave();
		Read(reader, data);
		return data;
	}

	public override void Read(ES2Reader reader, object c)
	{
		GunSave data = (GunSave)c;
		// Add your reader.Read calls here to read the data into the object.
		data.InMag = reader.Read<System.Int32>();
		data.BulletDelayRem = reader.Read<System.Single>();
		data.ReloadRem = reader.Read<System.Single>();

	}
	
	/* ! Don't modify anything below this line ! */
	public ES2UserType_GunSave():base(typeof(GunSave)){}
}