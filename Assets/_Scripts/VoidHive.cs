﻿using UnityEngine;
using System.Collections;

public class VoidHive : MonoBehaviour
{
	public int EnemiesFrom = 4;
	public int EnemiesTo = 15;
	public GameObject[] Types;

	void Start()
	{
		int CurEnemies = Random.Range(EnemiesFrom, EnemiesTo);

		for(int i = 0; i < CurEnemies; i++)
		{
			GameObject CurSpawn = Instantiate(Types[Random.Range(0, Types.Length - 1)], transform.position, Quaternion.identity) as GameObject;
		}
	}
}