﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using System;
using System.Collections;

public class DaySystem : MonoBehaviour
{
	public Day[] DaysActions;
	public UnityEvent OnNewDay;
	public int CurDay;
	
	public void NewDay()
	{
		CurDay++;
		FindObjectOfType<SaveSystem>().SaveGame();

		ExecuteCurDay();
		OnNewDay.Invoke();
	}
	public void ExecuteCurDay()
	{
		if(CurDay < DaysActions.Length)
		{
			DaysActions[CurDay].Execute.Invoke();
		}
	}
	public void SaveGame()
	{
		FindObjectOfType<SaveSystem>().SaveGame();
	}
	public void RestartLevel()
	{
		FWInput.Reset();
		SceneManager.LoadScene(0);
	}
	public void OnlyExit()
	{
		#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
		#else
			Application.Quit();
		#endif
	}
	public void DeleteSave()
	{
		FindObjectOfType<SaveSystem>().DeleteSave();
	}
	public void DeleteSaveNNew()
	{
		FindObjectOfType<SaveSystem>().DeleteSave();
		RestartLevel();
	}
}
[Serializable]
public class Day
{
	public string Note = "This day serves as tutorial";
	public UnityEvent Execute;
}