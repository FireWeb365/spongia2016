﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
using System.Collections;
using UnityEngine.EventSystems;

public class Player_InvenotryGUI : MonoBehaviour
{
	public GameObject ItemPrefab;
	public Text MoneyTXT;
	public RectTransform UserInv;
	public RectTransform MerchantInv;
	public int MerchantCID = 0;
	CanvasGroup CG;
	Player_Inventory PI;
	int[] CurMerchantList;
	int[] CurMerchantBOList = new int[0];
	bool MerchantOpen = false;

	void Awake()
	{
		CG = GetComponent<CanvasGroup>();
		PI = FindObjectOfType<Player_Inventory>();
	}
	void Start()
	{
		UpdateItems();
	}
	void Update()
	{
		MoneyTXT.text = PI.Items[0].Amount.ToString();

		if(PI.Updated)
		{
			PI.Updated = false;
			UpdateItems();
		}
	}
	void MultiGUIOpen()
	{
		OpenPGUI();
	}
	public void OpenPGUI()
	{
		UpdateItems();
	}
	public void OpenMerchant(int[] SellingList, int[] BuyoutList, int NCurrencyID)
	{
		MerchantCID = NCurrencyID;

		CurMerchantList = SellingList;
		CurMerchantBOList = BuyoutList;

		MerchantOpen = true;

		FindObjectOfType<MultiUIGUI>().OpenSwitch();
		FindObjectOfType<MultiUIGUI>().OpenTab(0);

		UpdateItems();
	}
	public void UpdateItems()
	{
		UserInv.anchorMax = new Vector2(MerchantOpen ? 0.5f : 1f, 1f);
		foreach(Transform child in UserInv.transform)
		{
			Destroy(child.gameObject);
		}
		foreach(Transform child in MerchantInv.transform)
		{
			Destroy(child.gameObject);
		}
		int i = 0;
		foreach(Item CItem in PI.Items)
		{
			int CItemID = int.Parse(i.ToString());
			if((CItem.Amount > 0 && i > 0) && !MerchantOpen || BOContains(CItemID))
			{
				GameObject CurItem = Instantiate(ItemPrefab, Vector3.zero, Quaternion.identity) as GameObject;

				CurItem.transform.SetParent(UserInv.transform);
				CurItem.transform.FindChild("Icon").GetComponent<Image>().sprite = CItem.Icon;
				CurItem.transform.FindChild("Name").GetComponent<Text>().text = CItem.Name;
				CurItem.transform.FindChild("Icon").FindChild("Amount").GetComponent<Text>().text = CItem.Amount.ToString();
				if(!CItem.CanDrop)
				{
					CurItem.transform.FindChild("DropOrSell").gameObject.SetActive(false);
				}
				else
				{
					if(MerchantOpen)
					{
						CurItem.transform.FindChild("DropOrSell").GetComponentInChildren<Text>().text = "Predaj";
						CurItem.GetComponentInChildren<Button>().onClick.AddListener(() => PlayerSell(CItemID, CurItem.GetComponentInChildren<InputField>()));
						CurItem.transform.FindChild("PriceTXT").GetComponentInChildren<Text>().text = "Cena za každý: " + CItem.SellPrice + PI.Items[MerchantCID].Name;
					}
					else
					{
						CurItem.GetComponentInChildren<Button>().onClick.AddListener(() => PlayerDrop(CItemID, CurItem.GetComponentInChildren<InputField>()));
					}
				}
			}
			i++;
		}
		if(MerchantOpen)
		{
			foreach(int CItemID in CurMerchantList)
			{
				Item CItem = PI.Items[CItemID];
				int SCItemID = int.Parse(CItemID.ToString());
				GameObject CurItem = Instantiate(ItemPrefab, Vector3.zero, Quaternion.identity) as GameObject;

				CurItem.transform.SetParent(MerchantInv.transform);
				CurItem.transform.FindChild("Icon").GetComponent<Image>().sprite = CItem.Icon;
				CurItem.transform.FindChild("Name").GetComponent<Text>().text = CItem.Name;
				CurItem.transform.FindChild("DropOrSell").GetComponentInChildren<Text>().text = "Kúp";
				CurItem.transform.FindChild("PriceTXT").GetComponentInChildren<Text>().text = "Cena za každý: " + CItem.BuyPrice + "$";
				CurItem.GetComponentInChildren<Button>().onClick.AddListener(() => PlayerBuy(SCItemID, CurItem.GetComponentInChildren<InputField>()));
			}
		}
	}
	public void PlayerSell(int ItemID, InputField Amount)
	{
		if(Amount.text.Length > 0)
		{
			int SellAMT = int.Parse(Amount.text);
			PI.Sell(ItemID, SellAMT);
		}
	}
	public void PlayerBuy(int ItemID, InputField Amount)
	{
		if(Amount.text.Length > 0)
		{
			int BuyAMT = int.Parse(Amount.text);
			PI.Buy(ItemID, BuyAMT, MerchantCID);
		}
	}
	public void PlayerDrop(int ItemID, InputField Amount)
	{
		if(Amount.text.Length > 0)
		{
			int DropAMT = int.Parse(Amount.text);
			PI.PlayerDrop(ItemID, DropAMT);
		}
	}
	bool BOContains(int ID)
	{
		foreach(int CID in CurMerchantBOList)
		{
			if(CID == ID)
			{
				return true;
			}
		}
		return false;
	}
	public void CloseMerchant()
	{
		MerchantOpen = false;
		CurMerchantList = new int[0];
		CurMerchantBOList = new int[0];
	}
}