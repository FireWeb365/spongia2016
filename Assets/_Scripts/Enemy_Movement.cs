﻿using UnityEngine;
using System.Collections;
using Pathfinding;

public class Enemy_Movement : MonoBehaviour
{
	public float Speed = 7f;
	public float AccSpeed = 4f;
	public float NextWaypointDistance = 0.6f;
	public float RoamDistance = 15f;
	public float RoamFrom = 5f;
	public float RoamTo = 11f;
	public Vector3 RoamOrigin;
	SpriteAnim SA;
	Enemy_Vision Vision;
	Seeker seeker;
	Rigidbody2D RB;
	Path path;
	int CurPoint;
	float ToRoamRem;

	void Awake()
	{
		Vision = GetComponent<Enemy_Vision>();
		RB = GetComponent<Rigidbody2D>();
		SA = GetComponent<SpriteAnim>();
		seeker = GetComponent<Seeker>();
		seeker.pathCallback += OnPathComplete;
		InvokeRepeating("UpdatePath", 0f, 0.6f);
	}
	void Start()
	{
		if(RoamOrigin == Vector3.zero)
		{
			RoamOrigin = transform.position;
		}
	}
	void Update()
	{
		ToRoamRem -= Time.deltaTime;
		if(!Vision.InterestP)
		{
			if(ToRoamRem <= 0f)
			{
				ToRoamRem = Random.Range(RoamFrom, RoamTo);

				seeker.StartPath(transform.position, RoamOrigin + (Vector3)(Vector2)(Random.insideUnitCircle * RoamDistance));
			}
		}
		if(path == null)
		{
			RB.velocity = Vector2.zero;
		}
		else
		{
			if(CurPoint >= path.vectorPath.Count)
			{
				path = null;
				Vision.InterestDecay = true;
				return;
			}
			Vector2 dir = (path.vectorPath[CurPoint] - transform.position).normalized;
			RB.velocity = new Vector2(Mathf.Lerp(0f, dir.x * Speed, AccSpeed), Mathf.Lerp(0f, dir.y * Speed, AccSpeed));

			if(Vector3.Distance(transform.position, path.vectorPath[CurPoint]) < NextWaypointDistance)
			{
				CurPoint++;
				return;
			}
		}
		if(Vector2.Distance(RB.velocity, Vector2.zero) < 0.4f)
		{
			SA.Play(0);
		}
		else
		{
			SA.Play(1);
		}
	}
	void UpdatePath()
	{
		if(Vision.InterestP)
		{
			seeker.StartPath(transform.position, Vision.LastPos);
		}
	}
	public void OnPathComplete(Path p)
	{
		if(!p.error)
		{
			path = p;
			CurPoint = 0;
		}
	}
}