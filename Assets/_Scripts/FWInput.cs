﻿using UnityEngine;
using System.Collections;

public class FWInput
{
	public static bool UIUp = false;
	public static bool DeadUp = false;
	public static bool TalkingSelf = false;
	public static bool MapZoomed = false;
	public static bool DialogueUp = false;

	public static bool Run()
	{
		return Input.GetKey(KeyCode.LeftShift);
	}
	public static bool CanPlayerIIWorld()
	{
		return !UIUp && !DeadUp && !TalkingSelf && !MapZoomed;
	}
	public static bool NextWeapon()
	{
		return Input.GetAxis("Mouse ScrollWheel") > 0f;
	}
	public static bool PrevWeapon()
	{
		return Input.GetAxis("Mouse ScrollWheel") < 0f;
	}
	public static bool PlayerMUISwitch()
	{
		return Input.GetKeyDown(KeyCode.Escape) && !DeadUp;
	}
	public static bool GetReload()
	{
		return Input.GetKeyDown(KeyCode.R);
	}
	public static bool ClosePopupOrContinueDialogue()
	{
		return Input.GetKeyDown(KeyCode.Space);
	}
	public static bool GetPrimaryFire()
	{
		return Input.GetMouseButton(0);
	}
	public static bool GetPrimaryFireTap()
	{
		return Input.GetMouseButtonDown(0);
	}
	public static Vector2 RelativeMousePos()
	{
		return Input.mousePosition - new Vector3(Screen.width / 2f, Screen.height / 2f, 0f);
	}
	public static Vector2 RelativeMousePosNorm()
	{
		return new Vector2(RelativeMousePos().x / (Screen.width / 2f), RelativeMousePos().y / (Screen.height / 2f));
	}
	public static void Reset()
	{
		UIUp = false;
		DeadUp = false;
		TalkingSelf = false;
		MapZoomed = false;
	}
}