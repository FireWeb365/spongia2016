﻿using UnityEngine;
using System.Collections;

public class Giberrish : MonoBehaviour
{
	public AudioSource AS;
	public AudioClip[] Gibbs;
	public float DelayCoef = 1.1f;

	void Start()
	{
		StartCoroutine(AIE());
	}
	IEnumerator AIE()
	{
		AudioClip CAC = Gibbs[Random.Range(0, Gibbs.Length - 1)];
		AS.PlayOneShot(CAC, 1f);
		yield return new WaitForSeconds(CAC.length * DelayCoef);
		StartCoroutine(AIE());
	}
}