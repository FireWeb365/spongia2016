﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class NestControl : MonoBehaviour
{
	public GameObject EnemyPrefab;
	public GameObject NestPrefab;
	public GameObject MarkerPrefab;
	public float MapSize = 100f;
	public Canvas MapObj;
	public int EnemiesFrom = 7;
	public int EnemiesTo = 15;

	public void CreateSpawnerAtTrans(Transform PosTarget)
	{
		CreateSpawner(PosTarget.position);
	}
	public void CreateSpawner(Vector3 PosTarget)
	{
		GameObject ThisNest = Instantiate(NestPrefab, PosTarget, Quaternion.identity) as GameObject;
		AddMarker(PosTarget);

		int ThisEnemyCount = Random.Range(EnemiesFrom, EnemiesTo);
		for(int i = 0; i < ThisEnemyCount; i++)
		{
			Instantiate(EnemyPrefab, PosTarget, Quaternion.identity);
		}
	}
	public void AddMarker(Vector3 MPos)
	{
		GameObject CMarker = Instantiate(MarkerPrefab, MapObj.transform.position + MPos / MapSize, Quaternion.identity) as GameObject;
		CMarker.transform.SetParent(MapObj.transform);
	}
	public void WipeMarkers()
	{
		foreach(Transform child in MapObj.transform)
		{
			GameObject.Destroy(child.gameObject);
		}
	}
	public void StartRandom()
	{
		InvokeRepeating("RandomSpawner", 6f, 9f);
	}
	public void RandomSpawner()
	{
		Vector3 RandomPos;
		RandomPos = new Vector3(Random.Range(-100f, 100f), Random.Range(-100f, 100f), 0f);
		while(Vector3.Distance(GameObject.FindWithTag("Player").transform.position, RandomPos) < 20f)
		{
			RandomPos = new Vector3(Random.Range(-100f, 100f), Random.Range(-100f, 100f), 0f);
		}
		CreateSpawner(RandomPos);
	}
}