﻿using UnityEngine;
using System.Collections;

public class Player_Core : MonoBehaviour
{
	public float WalkSpeed = 30f;
	public float RunSpeed = 55f;
	public bool CanFlip = true;
	public bool DidFlip;
	public GameObject Hand;
	public Transform SleepPos;
	//#Tutorial part
	public bool KilledEnemyAlr = true;
	public bool KilledAll = true;
	//#End of it
	Player_Movement PM = new Player_Movement();
	Rigidbody2D RB;
	SpriteRenderer SR;
	SpriteAnim SA;
	float HandX;

	void Awake()
	{
		RB = GetComponent<Rigidbody2D>();
		SR = GetComponent<SpriteRenderer>();
		SA = GetComponent<SpriteAnim>();
		HandX = Hand.transform.localPosition.x;
	}
	void Update()
	{
		PM.WalkSpeed = WalkSpeed;
		PM.SprintSpeed = RunSpeed;
		RB.velocity = PM.GetVel();
		SA.Play(Vector2.Distance(Vector2.zero, RB.velocity) > 0.3f ? 1 : 0);
		if(FWInput.CanPlayerIIWorld())
		{
			if(CanFlip)
			{
				DidFlip = FWInput.RelativeMousePosNorm().x >= 0f;
				SR.flipX = DidFlip;
				Vector3 HTL = Hand.transform.localPosition;
				HTL.x = DidFlip ? HandX : -HandX;
				Hand.transform.localPosition = HTL;
			}
		}
	}
	public void OnNewGame()
	{
		KilledEnemyAlr = false;
	}
	public void EnemyKill()
	{
		if(!KilledEnemyAlr)
		{
			KilledEnemyAlr = true;

			FindObjectOfType<DialogueSystem>().OpenDialogue(1);
		}
		if(AllEnemiesDead())
		{
			FWInput.CanPlayerIIWorld();
			FindObjectOfType<DialogueSystem>().OpenDialogue(0);
		}
	}
	public bool AllEnemiesDead()
	{
		Enemy_Health[] Enemies = FindObjectsOfType<Enemy_Health>();
		foreach(Enemy_Health CEnemy in Enemies)
		{
			if(!CEnemy.Dead)
			{
				return false;
			}
		}
		return true;
	}
	public void PlayerSleepPos()
	{
		transform.position = SleepPos.position;
	}
}