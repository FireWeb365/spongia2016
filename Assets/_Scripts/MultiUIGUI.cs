﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MultiUIGUI : MonoBehaviour
{
	public int CGUII = 0;
	public CanvasGroup[] CGs;
	Player_InvenotryGUI IGUI;
	CanvasGroup CG;

	void Awake()
	{
		CG = GetComponent<CanvasGroup>();
		IGUI = FindObjectOfType<Player_InvenotryGUI>();
	}
	void Update()
	{
		if(!FWInput.DeadUp)
		{
			Time.timeScale = !FWInput.UIUp && !FWInput.TalkingSelf ? 1f : 0f;
			AudioListener.pause = FWInput.UIUp;
			if(FWInput.PlayerMUISwitch())
			{
				OpenSwitch();
			}
			if(FWInput.UIUp)
			{
				if(Input.GetKeyDown(KeyCode.RightArrow))
				{
					if(CGs.Length > CGUII + 1)
					{
						CGUII++;
						UpdateOpen();
					}
				}
				else if(Input.GetKeyDown(KeyCode.LeftArrow))
				{
					if(CGUII > 0)
					{
						CGUII--;
						UpdateOpen();
					}
				}
			}
		}
	}
	public void OpenSwitch()
	{
		FWInput.UIUp = !FWInput.UIUp;
		FindObjectOfType<UnityStandardAssets.ImageEffects.BlurOptimized>().enabled = FWInput.UIUp;
		if(FWInput.UIUp)
		{
			IGUI.UpdateItems();

			CG.alpha = 1f;
			CG.blocksRaycasts = true;
			CG.interactable = true;
			UpdateOpen();
		}
		else
		{
			CG.alpha = 0f;
			CG.blocksRaycasts = false;
			CG.interactable = false;

			IGUI.CloseMerchant();
		}
	}
	public void OpenTab(int Index)
	{
		CGUII = Index;
		UpdateOpen();
	}
	public void UpdateOpen()
	{
		foreach(CanvasGroup CCG in CGs)
		{
			CCG.alpha = 0f;
			CCG.blocksRaycasts = false;
			CCG.interactable = false;
		}
		if(CGs.Length > CGUII)
		{
			CGs[CGUII].alpha = 1f;
			CGs[CGUII].blocksRaycasts = true;
			CGs[CGUII].interactable = true;
			CGs[CGUII].gameObject.SendMessage("MultiGUIOpen", SendMessageOptions.DontRequireReceiver);
		}
	}
}