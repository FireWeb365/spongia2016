﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_Guns : MonoBehaviour
{
	public int MyGun;
	public Gun[] GunsDB;
	public GameObject BulletPrefab;
	public GameObject SpawnPosition;
	public Image AmmoIndicator;
	public Image AmmoFill;
	public Text AmmoTXT;
	Player_Core Core;
	SpriteRenderer SR;
	ParticleSystem PS;
	AudioSource AS;
	Player_Inventory PI;

	void Awake()
	{
		Core = FindObjectOfType<Player_Core>();
		SR = GetComponentInChildren<SpriteRenderer>();
		PS = GetComponentInChildren<ParticleSystem>();
		PI = FindObjectOfType<Player_Inventory>();
		AS = GetComponent<AudioSource>();
	}
	void Update()
	{
		CurGun().BulletDelayRem -= Time.deltaTime;
		if(FWInput.CanPlayerIIWorld())
		{
			CheckWeaponChange();
			WeaponRot();

			if(CurGun().ReloadRem > 0f)
			{
				CurGun().ReloadRem -= Time.deltaTime;
			}
			else
			{
				if(ReloadReq() && PI.Items[CurGun().AmmoItemID].Amount > 0)
				{
					CurGun().ReloadRem = CurGun().ReloadTime;
					AS.PlayOneShot(CurGun().ReloadSound);

					if(PI.Items[CurGun().AmmoItemID].Amount >= CurGun().MagSize - CurGun().InMag)
					{
						PI.Items[CurGun().AmmoItemID].Amount -= CurGun().MagSize - CurGun().InMag;
						PI.Updated = true;
						CurGun().InMag = CurGun().MagSize;
					}
					else
					{
						CurGun().InMag += PI.Items[CurGun().AmmoItemID].Amount;
						PI.Items[CurGun().AmmoItemID].Amount = 0;
						PI.Updated = true;
					}
				}
				else
				{
					if(CurGun().Auto ? FWInput.GetPrimaryFire() : FWInput.GetPrimaryFireTap())
					{
						if(CurGun().InMag > 0 && CurGun().ReloadRem <= 0f)
						{
							if(CurGun().BulletDelayRem <= 0f)
							{
								CurGun().BulletDelayRem = CurGun().BulletDelay;

								AS.PlayOneShot(CurGun().ShotSound);
								PS.Play();

								foreach(Enemy_Vision CVision in FindObjectsOfType<Enemy_Vision>())
								{
									CVision.NoiseOnPos(transform.position);
								}

								CurGun().InMag--;

								for(int i = 0; i < CurGun().ProjectileAmount; i++)
								{
									GameObject CBullet = Instantiate(BulletPrefab, SpawnPosition.transform.position, transform.rotation * Quaternion.Euler(0f, 0f, UnityEngine.Random.Range(-CurGun().BulletSpread, CurGun().BulletSpread))) as GameObject;
									CBullet.GetComponent<Projectile>().Damage = CurGun().Damage;
									CBullet.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.up * CurGun().BulletSpeed);

									FindObjectOfType<Player_Camera>().Shake(0.1f);

									Destroy(CBullet, 1.6f);
								}
							}
						}
						else
						{
							AS.PlayOneShot(CurGun().EmptySound);
						}
					}
				}
			}
		}
		AmmoIndicator.fillAmount = 1f * CurGun().InMag / CurGun().MagSize;
		AmmoTXT.text = CurGun().InMag + " / " + PI.Items[CurGun().AmmoItemID].Amount;
	}
	public Gun CurGun()
	{
		return GunsDB[MyGun];
	}
	public bool ReloadReq()
	{
		return FWInput.GetReload() && CurGun().InMag < CurGun().MagSize;
	}
	public void CheckWeaponChange()
	{
		if(CurGun().ReloadRem > 0f) return;
		if(FWInput.NextWeapon())
		{
			int CGun = MyGun;
			bool Found = false;
			while(CGun < GunsDB.Length - 1 && !Found)
			{
				CGun++;
				if(PI.Items[GunsDB[CGun].ItemID].Amount > 0)
				{
					Found = true;
				}
			}
			if(Found)
			{
				MyGun = CGun;
			}
		}
		else if(FWInput.PrevWeapon())
		{
			int CGun = MyGun;
			bool Found = false;
			while(CGun > 0 && !Found)
			{
				CGun--;
				if(PI.Items[GunsDB[CGun].ItemID].Amount > 0)
				{
					Found = true;
				}
			}
			if(Found)
			{
				MyGun = CGun;
			}
		}
	}
	public void WeaponRot()
	{
		Vector3 diff = (Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position).normalized;
		float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90f);
		SR.transform.rotation = Quaternion.Euler(0f, 0f, -90f);
		transform.localScale = new Vector3(Core.DidFlip ? 1f : -1f, 1f, 1f);

		int CSI = Mathf.RoundToInt(rot_z / 45f) + 1;
		if(CSI > 2)
		{
			CSI = 2;
		}
		if(CSI < 0)
		{
			CSI = 0;
		}
		SR.flipX = Core.DidFlip;
		SR.flipY = !Core.DidFlip;
		if(!Core.DidFlip)
		{
			if(rot_z < 0f && rot_z > -150f)
			{
				CSI = 0;
			}
			else if(rot_z > 0f && rot_z < 150f)
			{
				CSI = 2;
			}
			else
			{
				CSI = 1;
			}
		}
		SR.sprite = CurGun().GunIMG[CSI];
		AmmoIndicator.sprite = CurGun().BulletsIMG;
		AmmoFill.sprite = CurGun().BulletsIMG;
	}
	public GunSave[] GetForSave()
	{
		List<GunSave> ExportGuns = new List<GunSave>();

		foreach(Gun CGun in GunsDB)
		{
			ExportGuns.Add(new GunSave(CGun.InMag, CGun.BulletDelayRem, CGun.ReloadRem));
		}

		return ExportGuns.ToArray();
	}
	public void SetSave(GunSave[] Save)
	{
		int i = 0;
		foreach(GunSave CLoad in Save)
		{
			GunsDB[i].InMag = CLoad.InMag;
			GunsDB[i].BulletDelayRem = CLoad.BulletDelayRem;
			GunsDB[i].ReloadRem = CLoad.ReloadRem;

			i++;
		}
	}
}
[Serializable]
public class Gun
{
	public string Name = "Revolver";
	public int MagSize = 6;
	public int InMag = 0;
	public int Damage = 7;
	public int ItemID = 1;
	public int AmmoItemID = 1;
	public AudioClip ShotSound;
	public AudioClip ReloadSound;
	public AudioClip EmptySound;
	public Sprite[] GunIMG;
	public Sprite BulletsIMG;
	public float BulletSpeed = 9f;
	public float BulletSpread = 0.3f;
	public int ProjectileAmount = 1;
	public bool Auto = false;
	public float BulletDelay = 0.3f;
	public float BulletDelayRem = 0f;
	public float ReloadTime = 5f;
	public float ReloadRem = 0f;
}
[Serializable]
public class GunSave
{
	public int InMag = 0;
	public float BulletDelayRem = 0f;
	public float ReloadRem = 0f;

	public GunSave(int InMag = 0, float BulletDelayRem = 0f, float ReloadRem = 0f)
	{
		this.InMag = InMag;
		this.BulletDelayRem = BulletDelayRem;
		this.ReloadRem = ReloadRem;
	}
}