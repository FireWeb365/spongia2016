﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour
{
	public int MaxHealth = 6;
	public int CurHealth = 0;
	public bool Dead = false;

	void Awake()
	{
		CurHealth = MaxHealth;
	}
	public virtual void Damage(int AMT = 1)
	{
		if(!Dead)
		{
			CurHealth -= AMT;

			CheckDeath();
		}
	}
	public virtual void Kill()
	{
		if(!Dead)
		{
			CurHealth = 0;
			CheckDeath();
		}
	}
	public virtual void CheckDeath()
	{
		if(CurHealth <= 0 && !Dead)
		{
			Die();
		}
	}
	public virtual void Die()
	{
		Dead = true;
	}
}