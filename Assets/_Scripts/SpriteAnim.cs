﻿using UnityEngine;
using System.Collections;

public class SpriteAnim : MonoBehaviour
{
	public SpriteAnimation[] Animations;
	public int CurrentAnim;
	float DelayRem = 0f;
	int CFrame = 0;
	SpriteRenderer SR;

	void Awake()
	{
		SR = GetComponent<SpriteRenderer>();
	}
	void Update()
	{
		DelayRem -= Time.deltaTime;
		if(DelayRem <= 0f)
		{
			DelayRem = Animations[CurrentAnim].Delay;
			if(Animations[CurrentAnim].Random)
			{
				CFrame = Random.Range(0, Animations[CurrentAnim].Sprites.Length);
			}
			else
			{
				CFrame++;
				if(CFrame > Animations[CurrentAnim].Sprites.Length - 1)
				{
					CFrame = 0;
				}
			}
			SR.sprite = Animations[CurrentAnim].Sprites[CFrame];
		}
	}
	public void Play(int i)
	{
		if(i != CurrentAnim)
		{
			CFrame = 0;
			DelayRem = 0f;
			CurrentAnim = i;
		}
	}
}
[System.Serializable]
public class SpriteAnimation
{
	public string Name = "Idle";
	public float Delay = 0.2f;
	public Sprite[] Sprites;
	public bool Random = false;
}