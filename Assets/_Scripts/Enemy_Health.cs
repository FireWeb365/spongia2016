﻿using UnityEngine;
using System.Collections;

public class Enemy_Health : Health
{
	public AudioClip OuchSound;
	public AudioClip DieSound;
	AudioSource AS;

	void Start()
	{
		AS = GetComponent<AudioSource>();
	}
	public override void Die()
	{
		if(!Dead)
		{
			base.Die();

			AS.PlayOneShot(DieSound);

			FindObjectOfType<Player_Core>().EnemyKill();
			GetComponent<Enemy_Movement>().enabled = false;
			GetComponent<Enemy_Melee>().enabled = false;
			GetComponent<Enemy_Vision>().enabled = false;
			GetComponent<Rigidbody2D>().isKinematic = true;
			GetComponent<Collider2D>().isTrigger = true;
			GetComponent<SpriteRenderer>().sortingLayerName = "DeadBody";
			GetComponent<SpriteAnim>().Play(2);
		}
	}
}