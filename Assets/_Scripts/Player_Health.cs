﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Player_Health : Health
{
	public AudioClip OuchSound;
	public AudioClip DieSound;
	public Image HealthIMG;
	public CanvasGroup DeadScreen;
	AudioSource AS;

	void Start()
	{
		AS = GetComponent<AudioSource>();
	}
	public override void Damage(int DMG = 1)
	{
		if(!Dead)
		{
			base.Damage(DMG);

			AS.PlayOneShot(OuchSound);
			HealthIMG.fillAmount = 1f * CurHealth / MaxHealth;
		}
	}
	public override void Die()
	{
		if(!Dead)
		{
			base.Die();
			//AS.PlayOneShot(DieSound);
			//TODO: Switch audio channels to only dead one to play and dont pause audiolistener
			FWInput.UIUp = false;
			FindObjectOfType<MultiUIGUI>().UpdateOpen();
			FWInput.DeadUp = true;
			Time.timeScale = 0f;
			DeadScreen.alpha = 1f;
			DeadScreen.interactable = true;
			DeadScreen.blocksRaycasts = true;
			AudioListener.pause = true;
			FindObjectOfType<UnityStandardAssets.ImageEffects.BlurOptimized>().enabled = true;
		}
	}
	public void HealUp()
	{
		CurHealth = MaxHealth;
	}
}