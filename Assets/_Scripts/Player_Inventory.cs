﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player_Inventory : MonoBehaviour
{
	public GameObject ItemDropPrefab;
	public Item[] Items;
	public bool Updated = false;

	public bool AddItem(int ItemID, int Amount = 1)
	{
		Items[ItemID].Amount += Amount;

		Updated = true;
		return true;
	}
	public void MakeItemDrop(Vector3 DropPos, int ItemID, int Amount = 1)
	{
		ItemDrop CurDrop = (Instantiate(ItemDropPrefab, DropPos, Quaternion.identity) as GameObject).GetComponent<ItemDrop>();
		CurDrop.ItemID = ItemID;
		CurDrop.Amount = Amount;
		CurDrop.TTipStr = "Pick up " + Amount + (ItemID == 0 ? "$..." : ("x" + Items[ItemID].Name + "..."));
	}
	public void PlayerDrop(int ItemID, int Amount = 1)
	{
		Updated = true;

		if(Amount > 0 && Items[ItemID].Amount >= Amount)
		{
			Items[ItemID].Amount -= Amount;

			Updated = true;

			ItemDrop CurDrop = (Instantiate(ItemDropPrefab, GameObject.FindWithTag("Player").transform.position, Quaternion.identity) as GameObject).GetComponent<ItemDrop>();
			CurDrop.ItemID = ItemID;
			CurDrop.Amount = Amount;
			CurDrop.TTipStr = "Pick up " + Amount + (ItemID == 0 ? "$..." : ("x" + Items[ItemID].Name + "..."));
		}
	}
	public bool Buy(int ItemID, int Amount, int CurrencyID = 0)
	{
		if(Items[CurrencyID].Amount > Amount * Items[ItemID].BuyPrice && Amount > 0)
		{
			Updated = true;
			
			Items[CurrencyID].Amount -= Amount * Items[ItemID].BuyPrice;
			Items[ItemID].Amount += Amount;

			return true;
		}
		else
		{
			return false;
		}
	}
	public bool Sell(int ItemID, int Amount)
	{
		if(Amount > 0 && Items[ItemID].Amount >= Amount)
		{
			Updated = true;

			Items[ItemID].Amount -= Amount;
			Items[0].Amount += Amount * Items[ItemID].SellPrice;

			return true;
		}
		else
		{
			return false;
		}
	}
	public int[] GetForSave()
	{
		List<int> ExportSave = new List<int>();

		foreach(Item CItem in Items)
		{
			ExportSave.Add(CItem.Amount);
		}

		return ExportSave.ToArray();
	}
	public void SetSave(int[] Amounts)
	{
		int i = 0;
		foreach(int CAmount in Amounts)
		{
			Items[i].Amount = CAmount;

			i++;
		}
	}
}
[System.Serializable]
public class Item
{
	public string Name = "Shotgun shell";
	public Sprite Icon;
	public int Amount;
	public int BuyPrice = 5;
	public int SellPrice = 2;
	public bool CanDrop = true;
}