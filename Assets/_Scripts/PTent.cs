﻿using UnityEngine;
using System.Collections;

public class PTent : Interactable
{
	public override void Interact()
	{
		base.Interact();

		bool Can = true;
		if(FindObjectOfType<Scientist>())
		{
			if(!FindObjectOfType<Scientist>().Happy)
			{
				Can = false;
			}
		}
		if(!FindObjectOfType<Player_Core>().AllEnemiesDead())
		{
			Can = false;
		}

		if(Can)
		{
			FindObjectOfType<DaySystem>().NewDay();
		}
		else
		{
			FindObjectOfType<DialogueSystem>().OpenDialogue(9);
		}
	}
}