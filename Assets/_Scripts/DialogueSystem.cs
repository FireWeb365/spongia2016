﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;

public class DialogueSystem : MonoBehaviour
{
	public CanvasGroup DCG;
	public Dialogue[] Dialogues;
	public Text DiagTXT;
	int CurLine;
	int DialogueID;

	void Update()
	{
		if(FWInput.ClosePopupOrContinueDialogue())
		{
			ContinueDialogue();
		}
		DCG.interactable = FWInput.TalkingSelf;
		DCG.blocksRaycasts = FWInput.TalkingSelf;
		if(FWInput.TalkingSelf)
		{
			DCG.alpha = Mathf.Lerp(DCG.alpha, 1f, Time.unscaledDeltaTime * 3f);
		}
		else
		{
			DCG.alpha = Mathf.Lerp(DCG.alpha, 0f, Time.unscaledDeltaTime * 3f);
		}
		DiagTXT.text = CLine();
	}
	public void OpenDialogue(int DID)
	{
		DialogueID = DID;
		CurLine = 0;
		FWInput.TalkingSelf = true;
	}
	public void ContinueDialogue()
	{
		if(CurLine < CurD().Lines.Length - 1)
		{
			CurLine++;
		}
		else
		{
			CurD().OnEnd.Invoke();
			CloseDialogue();
		}
	}
	public void CloseDialogue()
	{
		FWInput.TalkingSelf = false;
	}
	public Dialogue CurD()
	{
		return Dialogues[DialogueID];
	}
	public string CLine()
	{
		return CurD().Lines[CurLine];
	}
}
[System.Serializable]
public class Dialogue
{
	public string EditorTitle = "Tutorial diag";
	public bool Monologue = false;
	public string[] Lines;
	public UnityEvent OnEnd;
}