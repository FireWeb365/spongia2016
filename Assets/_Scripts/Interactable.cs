﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider2D))]
public class Interactable : MonoBehaviour
{
	public bool IsInteractable = true;
	public float UDoTime = 0.65f;
	public string TTipStr = "Something isn't functioning right...";
	InteractTTip TTG;
	float UDoCur = 0f;
	bool PIn;

	void Awake()
	{
		TTG = FindObjectOfType<InteractTTip>();
	}
	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.tag == "Player" && !PIn && IsInteractable)
		{
			Enter();
		}
	}
	void OnTriggerExit2D(Collider2D col)
	{
		if(col.tag == "Player" && PIn)
		{
			Exit();
		}
	}
	public virtual void Update()
	{
		if(PIn)
		{
			if(FWInput.CanPlayerIIWorld())
			{
				Stay();
			}
		}
	}
	public virtual void Enter()
	{
		PIn = true;
		TTG.TTipTXT = TTipStr;
		TTG.TTipEnabled = true;
		TTG.FillAmt = 1f;
	}
	public virtual void Exit()
	{
		PIn = false;
		UDoCur = 0f;
		TTG.FillAmt = 0f;
		TTG.TTipEnabled = false;
	}
	public virtual void Stay()
	{
		if(Input.GetKey(KeyCode.E) && IsInteractable)
		{
			DoInteract();
		}
		else
		{
			NoInteract();
		}
	}
	public virtual void DoInteract()
	{
		UDoCur += Time.fixedDeltaTime;
		TTG.FillAmt = UDoCur / UDoTime;

		if(UDoCur >= UDoTime)
		{
			UDoCur = 1f;
			if(FWInput.CanPlayerIIWorld())
			{
				UDoCur = 0f;
				Interact();
			}
		}
	}
	public virtual void NoInteract()
	{
		UDoCur = 0f;
		TTG.FillAmt = 0f;
	}
	public virtual void Interact()
	{
		
	}
}