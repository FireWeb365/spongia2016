﻿using UnityEngine;
using System.Collections;

public class ItemDrop : Interactable
{
	public int ItemID;
	public int Amount;

	public override void Interact()
	{
		base.Interact();

		if(FindObjectOfType<Player_Inventory>().AddItem(ItemID, Amount))
		{
			Destroy(gameObject);
		}

		Exit();
	}
}