﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SSScript : MonoBehaviour
{
	AsyncOperation async;

	void Start()
	{
		Time.timeScale = 1f;
		Hashtable fht = new Hashtable();
		fht.Add("from", 0f);
		fht.Add("to", 1f);
		fht.Add("time", 1.6f);
		fht.Add("delay", 0f);
		fht.Add("onupdate", "UpdateSFade");
		Hashtable sht = new Hashtable();
		sht.Add("from", 1f);
		sht.Add("to", 0f);
		sht.Add("time", 1f);
		sht.Add("delay", 2f);
		sht.Add("onupdate", "UpdateSFade");
		iTween.ValueTo(gameObject, fht);
		iTween.ValueTo(gameObject, sht);
		StartCoroutine(LoadMenu());
	}
	void UpdateSFade(float NewVal)
	{
		GetComponent<Light>().intensity = NewVal;
	}
	IEnumerator LoadMenu()
	{
		async = SceneManager.LoadSceneAsync("Game", LoadSceneMode.Additive);
		async.allowSceneActivation = false;

		yield return new WaitForSeconds(3f);
		async.allowSceneActivation = true;
		while(!async.isDone)
		{
			yield return new WaitForEndOfFrame();
		}
		
		SceneManager.SetActiveScene(SceneManager.GetSceneByName("Game"));
		SceneManager.UnloadScene(SceneManager.GetSceneByName("SplashScreen"));
	}
}