﻿using UnityEngine;
using System.Collections;

public class Enemy_Melee : MonoBehaviour
{
	public float AttackRange = 1.6f;
	public float AttackCooldown = 3f;
	public int AttackDamage = 5;
	public ParticleSystem PS;
	public AudioClip AttackSound;
	GameObject Player;
	float CurAtkCooldown;

	void Awake()
	{
		Player = GameObject.FindWithTag("Player");
	}
	void Update()
	{
		CurAtkCooldown -= Time.deltaTime;
		if(Vector3.Distance(transform.position, Player.transform.position) <= AttackRange && CurAtkCooldown <= 0f)
		{
			CurAtkCooldown = AttackCooldown;

			Player.GetComponent<Player_Health>().Damage(AttackDamage);
			if(PS != null)
			{
				PS.Play();
			}
			if(GetComponent<AudioSource>() != null && AttackSound != null)
			{
				GetComponent<AudioSource>().PlayOneShot(AttackSound);
			}
		}
	}
	void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawSphere(transform.position, AttackRange);
	}
}