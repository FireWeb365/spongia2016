﻿using UnityEngine;
using System.Collections;

public class Enemy_Vision : MonoBehaviour
{
	public bool SeePlayer = false;
	public bool InterestP = false;
	public float DetectRange = 11f;
	public Vector3 LastPos;
	public float InterestLength = 5f;
	public float HearRange = 26f;
	[HideInInspector]
	public bool InterestDecay;
	GameObject Player;
	float InterestRem;

	void Awake()
	{
		Player = GameObject.FindWithTag("Player");
	}
	void Update()
	{
		if(PInRange())
		{
			LastPos = Player.transform.position;
			SeePlayer = true;
			InterestP = true;
			InterestDecay = false;
			InterestRem = InterestLength;
		}
		else
		{
			SeePlayer = false;
			if(InterestDecay)
			{
				InterestRem -= Time.deltaTime;
				if(InterestRem <= 0f)
				{
					InterestP = false;
				}
			}
		}
	}
	public bool PInRange()
	{
		return Vector2.Distance(Player.transform.position, transform.position) < DetectRange;
	}
	public void InterestEnemy(Vector3 IntPos)
	{
		if(!SeePlayer)
		{
			LastPos = IntPos;
			InterestP = true;
			InterestDecay = false;
			InterestRem = InterestLength;
		}
	}
	public void NoiseOnPos(Vector3 NoisePos)
	{
		if(Vector2.Distance(transform.position, NoisePos) <= HearRange)
		{
			InterestEnemy(NoisePos);
		}
	}
}