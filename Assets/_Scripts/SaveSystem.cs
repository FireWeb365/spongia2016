﻿using UnityEngine;
using UnityEngine.Events;

public class SaveSystem : MonoBehaviour
{
	public bool TryToLoadOnStart;
	public UnityEvent OnNewGame;
	public UnityEvent OnLoadGame;

	void Awake()
	{
		if(TryToLoadOnStart && ES2.Exists("Main.fw"))
		{
			LoadGame();
		}
		else
		{
			Debug.Log("New game!");
			OnNewGame.Invoke();
		}
	}
	public void SaveGame()
	{
		Debug.Log("Saving the game...");
		using(ES2Writer writer = ES2Writer.Create("Main.fw"))
		{
			writer.Write(FindObjectOfType<Player_Inventory>().GetForSave(), "PlayerInvItems");
			writer.Write(FindObjectOfType<Player_Guns>().GetForSave(), "PlayerGunsGunsDB");
			writer.Write(FindObjectOfType<DaySystem>().CurDay, "DaySystemCurDay");

			writer.Save();
		}
	}
	public void LoadGame()
	{
		Debug.Log("Loading the game...");
		using(ES2Reader reader = ES2Reader.Create("Main.fw"))
		{
			FindObjectOfType<Player_Inventory>().SetSave(reader.ReadArray<int>("PlayerInvItems"));
			FindObjectOfType<Player_Guns>().SetSave(reader.ReadArray<GunSave>("PlayerGunsGunsDB"));
			FindObjectOfType<DaySystem>().CurDay = reader.Read<int>("DaySystemCurDay");
		}
		OnLoadGame.Invoke();
	}
	public void DeleteSave()
	{
		ES2.Delete("Main.fw");
	}
}