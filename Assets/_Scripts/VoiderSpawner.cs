﻿using UnityEngine;
using System.Collections;

public class VoiderSpawner : MonoBehaviour
{
	public bool TSpawn;
	public float SpawnEvery = 20f;
	public float XWidth = 30f;
	public float YWidth = 30f;
	public GameObject VoidPrefab;

	void Start()
	{
		if(TSpawn)
		{
			InvokeRepeating("CreateSpawnerRandom", 3f, SpawnEvery);
		}
	}
	public void CreateSpawnerRandom()
	{
		CreateSpawner(new Vector3(Random.Range(-XWidth, XWidth), Random.Range(-YWidth, YWidth), 0f));
	}
	public void CreateSpawnerAtTrans(Transform PosTarget)
	{
		CreateSpawner(PosTarget.position);
	}
	public void CreateSpawner(Vector3 PosTarget)
	{
		GameObject NewGO = Instantiate(VoidPrefab, PosTarget, Quaternion.identity) as GameObject;
	}
	void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawCube(transform.position, new Vector3(XWidth * 2f, YWidth * 2f, 1f));
	}
}