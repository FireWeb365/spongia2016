﻿using UnityEngine;
using System.Collections;

public class Player_Movement
{
	public float AccelerateSpeed = 1.3f;
	public float WalkSpeed;
	public float SprintSpeed;

	public Vector2 GetVel()
	{
		float CurSpeed = FWInput.CanPlayerIIWorld() ? FWInput.Run() ? SprintSpeed : WalkSpeed : 0f;
		
		return new Vector2(Mathf.Lerp(0f, Input.GetAxis("Horizontal") * CurSpeed, AccelerateSpeed), Mathf.Lerp(0f, Input.GetAxis("Vertical") * CurSpeed, AccelerateSpeed));
	}
}