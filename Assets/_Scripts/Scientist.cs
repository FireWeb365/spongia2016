﻿using UnityEngine;
using System.Collections;

public class Scientist : Interactable
{
	public int GameState = 0;
	public GameObject ArriveObj;
	public bool Happy = true;
	
	public void ScientistArrive()
	{
		ArriveObj.SetActive(true);
	}
	public override void Interact()
	{
		base.Interact();

		if(GameState == 0)	//Intro day
		{
			FindObjectOfType<DialogueSystem>().OpenDialogue(3);
			Happy = true;
		}
		else if(GameState == 1)	//Talk about need of CPUs
		{
			GameState = 2;
			Happy = false;
			FindObjectOfType<DialogueSystem>().OpenDialogue(4);
		}
		else if(GameState == 2)	//Give me CPUs
		{
			if(FindObjectOfType<Player_Inventory>().Buy(7, 1, 1))
			{
				GameState = 3;
				FindObjectOfType<DialogueSystem>().OpenDialogue(5);
				Happy = true;
			}
			else
			{
				FindObjectOfType<DialogueSystem>().OpenDialogue(6);
				Happy = false;
			}
		}
		else if(GameState == 3) //CPUs done
		{
			Happy = true;
			FindObjectOfType<DialogueSystem>().OpenDialogue(7);
		}
	}
	public void SetGameState(int NGameState)
	{
		GameState = NGameState;
		if(GameState == 1 || GameState == 2)
		{
			Happy = false;
		}
		else
		{
			Happy = true;
		}
	}
}