﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
	public int Damage = 6;

	void OnCollisionEnter2D(Collision2D col)
	{
		Destroy(gameObject);

		if(col.collider.GetComponent<Health>())
		{
			col.collider.GetComponent<Health>().Damage(Damage);
		}
	}
}