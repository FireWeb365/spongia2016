﻿using UnityEngine;
using System.Collections;

public class NPC : Interactable
{
	public bool Merchant;
	public int[] SellingList;
	public int[] BuyoutList;
	public int CurDiag = 0;
	public int CurrencyID = 0;

	public override void Interact()
	{
		base.Interact();

		if(Merchant)
		{
			FindObjectOfType<Player_InvenotryGUI>().OpenMerchant(SellingList, BuyoutList, CurrencyID);
		}
	}
	public void SetDialogue(int NID)
	{
		CurDiag = NID;
	}
}