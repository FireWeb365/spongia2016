﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class InteractTTip : MonoBehaviour
{
	[Range(0f, 1f)]
	public float FillAmt = 0f;
	public string TTipTXT;
	public bool TTipEnabled = false;
	public RectTransform Fill;
	CanvasGroup CG;
	Text TXT;

	void Awake()
	{
		CG = GetComponent<CanvasGroup>();
		TXT = GetComponentInChildren<Text>();
	}
	void Update()
	{
		Fill.anchorMax = new Vector2(FillAmt, Fill.anchorMax.y);
		CG.alpha = TTipEnabled ? 1f : 0f;
		CG.interactable = TTipEnabled;
		CG.blocksRaycasts = TTipEnabled;
		TXT.text = "'E' - " + TTipTXT;
	}
	public bool Working()
	{
		return FillAmt != 0f;
	}
}