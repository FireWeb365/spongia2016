﻿using UnityEngine;
using System.Collections;

public class Player_Camera : MonoBehaviour
{
	public float CameraFollowSpeed = 6f;
	public float AimAMT = 3f;
	public GameObject ApplyPosition;
	public GameObject ApplyShake;
	public Vector3 Pivot;
	public float CantBeyondX = 100f;
	public float CantBeyondY = 100f;
	public Vector3 TalkPivot;
	public float DefZoom = 8f;
	public float TalkZoom = 0.6f;
	public GameObject Map;
	public float MapZoom = 0.3f;
	Camera Cam;

	void Awake()
	{
		Cam = Camera.main;
	}
	void Update()
	{
		if(!FWInput.TalkingSelf && !FWInput.MapZoomed)
		{
			if(FWInput.CanPlayerIIWorld())
			{
				Vector3 WatchPos = FWInput.RelativeMousePosNorm() * AimAMT;
				WatchPos += transform.position;
				WatchPos += Pivot;
				WatchPos.x = WatchPos.x > CantBeyondX ? CantBeyondX : WatchPos.x;
				WatchPos.x = WatchPos.x < -CantBeyondX ? -CantBeyondX : WatchPos.x;
				WatchPos.y = WatchPos.y > CantBeyondY ? CantBeyondY : WatchPos.y;
				WatchPos.y = WatchPos.y < -CantBeyondY ? -CantBeyondY : WatchPos.y;
				ApplyPosition.transform.localPosition = WatchPos;
				Cam.orthographicSize = Mathf.Lerp(Cam.orthographicSize, DefZoom, Time.deltaTime);
			}
		}
		else
		{
			if(FWInput.MapZoomed)
			{
				ApplyPosition.transform.position = Vector3.Lerp(ApplyPosition.transform.position, Map.transform.position + Pivot, Time.deltaTime);
				Cam.orthographicSize = Mathf.Lerp(Cam.orthographicSize, MapZoom, Time.deltaTime);
			}
			else if(FWInput.TalkingSelf)
			{
				ApplyPosition.transform.localPosition = Vector3.Lerp(ApplyPosition.transform.localPosition, TalkPivot + transform.position, Time.deltaTime);
				Cam.orthographicSize = Mathf.Lerp(Cam.orthographicSize, TalkZoom, Time.deltaTime);
			}
		}
	}
	public void Shake(float Power)
	{
		iTween.ShakePosition(ApplyShake, new Vector3(1f, 1f) * Power, Power);
	}
}