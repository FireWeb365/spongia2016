﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class MakeGrid : MonoBehaviour
{
	public int ManualCount = 10;
	public float Size = 1;
	public bool Execute = false;
	public bool ClearAll = false;
	SpriteRenderer sprite;

	void Update()
	{
		if(Execute)
		{
			Execute = false;
			sprite = GetComponent<SpriteRenderer>();
			GameObject childPrefab = new GameObject();
			childPrefab.transform.localScale = Vector3.one * Size;
			childPrefab.transform.rotation = transform.rotation;

			SpriteRenderer childSprite = childPrefab.AddComponent<SpriteRenderer>();
			childSprite.sortingLayerID = sprite.sortingLayerID;
			childSprite.sortingOrder = sprite.sortingOrder;
			childPrefab.transform.position = transform.position;
			childSprite.sprite = sprite.sprite;

			GameObject child;
			for(int i = 0; i < ManualCount; i++)
			{
				child = Instantiate(childPrefab) as GameObject;
				child.transform.parent = transform;
				child.transform.localPosition = new Vector3(i * Size, 0f, 0f);
			}

			DestroyImmediate(childPrefab);
			sprite.enabled = false;
		}
		if(ClearAll)
		{
			ClearAll = false;
			foreach(Transform ToDes in transform)
			{
				DestroyImmediate(ToDes.gameObject);
			}
		}
	}
}