﻿using UnityEngine;
using System.Collections;

public class CityMap : Interactable
{
	public override void Interact()
	{
		base.Interact();

		FWInput.MapZoomed = true;
	}
	public override void Update()
	{
		base.Update();

		if(FWInput.MapZoomed && FWInput.ClosePopupOrContinueDialogue())
		{
			FWInput.MapZoomed = false;
		}
	}
}